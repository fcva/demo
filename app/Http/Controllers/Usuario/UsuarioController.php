<?php

namespace App\Http\Controllers\Usuario;

use PDF;
use App\Models\User;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $usuarios = User::filtrar($request->usuario)->get();

        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * param  \Illuminate\Http\Request  $request
     * return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $request->request->add([
            'password' => Hash::make($request->password)
        ]);

        User::create($request->all());

        return redirect()->route('usuarios.index')->with('mensaje', 'Los datos se guardaron correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);

        return view('usuarios.edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * param  \Illuminate\Http\Request  $request
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $request->request->add([
            'password' => Hash::make($request->password)
        ]);

        $user->update($request->all());

        return redirect()->route('usuarios.index')->with('mensaje', 'Los datos se han actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);

        return view('usuarios.destroy', compact('usuario'));
    }

    public function delete(User $user)
    {
        $user->delete();

        return redirect()->route('usuarios.index')->with('mensaje', 'Los datos se han eliminado correctamente.');
    }

    /**
     * Exportar to Excel
     */
    public function exportarExcel()
    {
        return Excel::download(new UsersExport, 'usuarios.xlsx');
    }

    /**
     * Exportar to PDF
     */
    public function exportarPdf()
    {
        $usuarios = User::get();

        $pdf = PDF::loadView('usuarios.exportarPdf', compact('usuarios'));

        return $pdf->download('usuarios.pdf');
    }
}
