<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Model,SoftDeletes};

class Persona extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'personas';

    protected $guarded = [];

    /**
     * Relación inversa one to one con el modelo User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
