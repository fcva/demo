<?php

namespace App\Models;

use App\Models\Persona;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\{SoftDeletes,Builder};

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'rol',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Filtrar datos 
     */
    public function scopeFiltrar(Builder $builder, $usuario)
    {
        if (trim($usuario) != '') {

            $builder->where('name', 'LIKE', "%$usuario%")->orWhere('email', 'LIKE', "%$usuario%");
        }
    }


    /**
     * Relación uno a muchos con el modelo Persona
     */
    public function persona()
    {
        return $this->hasOne(Persona::class);
    }
}
