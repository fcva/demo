<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Usuario\UsuarioController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/usuarios/index', [UsuarioController::class, 'index'])->name('usuarios.index')->middleware('auth');

Route::get('/usuarios/create', [UsuarioController::class, 'create'])->name('usuarios.create');

Route::post('/usuarios/store', [UsuarioController::class, 'store'])->name('usuarios.store');

Route::get('/usuarios/edit/{id}', [UsuarioController::class, 'edit'])->name('usuarios.edit')->middleware('auth');

Route::put('/usuarios/update/{user}', [UsuarioController::class, 'update'])->name('usuarios.update');

Route::get('/usuarios/destroy/{id}', [UsuarioController::class, 'destroy'])->name('usuarios.destroy');

Route::delete('/usuarios/delete/{user}', [UsuarioController::class, 'delete'])->name('usuarios.delete');

Route::get('/usuarios/exportar/excel', [UsuarioController::class, 'exportarExcel'])->name('usuarios.exportar.excel');

Route::get('/usuarios/exportar/pdf', [UsuarioController::class, 'exportarPdf'])->name('usuarios.exportar.pdf');