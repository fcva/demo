@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Usuarios
                </div>

                <div class="card-body">

                    @if(session('mensaje'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            {{ session('mensaje') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    {{-- @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso!</strong>
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif --}}
                </div>

                <div class="card-body">
                    <div class="row">

                        <div class="col-md-6">

                            <a href="{{ route('usuarios.create') }}" class="btn btn-success text-white">Nuevo</a>

                            <a href="{{ route('usuarios.exportar.excel') }}" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="{{ route('usuarios.exportar.pdf') }}" class="btn btn-outline-danger">Exportar PDF</a>
                            
                        </div>

                        <div class="col-md-6">
                            <form action="{{ route('usuarios.index') }}" method="GET">

                                <div class="input-group mb-3">
                                    <input type="text" name="usuario" class="form-control" autocomplete="off">
                                    <button class="btn btn-primary text-white" type="submit">Buscar</button>
                                </div>

                            </form>
                        </div>
                        
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre de usuario</th>
                                <th scope="col">Correo electrónico</th>
                                <th>Password</th>
                                <th scope="col">Rol</th>
                                <th>editar</th>
                                <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $key => $usuario)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $usuario->name }}</td>
                                    <td>{{ $usuario->email }}</td>
                                    <td>{{ $usuario->password }}</td>
                                    <td>{{ $usuario->rol }}</td>
                                    <td>
                                        <a href="{{ route('usuarios.edit', $usuario->id) }}" class="btn btn-warning">Editar</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('usuarios.destroy', $usuario->id) }}" class="btn btn-danger text-white">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach                             
                        </tbody>
                    </table>                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection