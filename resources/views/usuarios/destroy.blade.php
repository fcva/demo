@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Eliminar usuario
                    </div>

                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Aviso!</strong>
                                @foreach ($errors->all() as $error)
                                    {{ $error }}
                                @endforeach
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @endif
                    </div>

                    <div class="card-body">

                        <form action="{{ route('usuarios.delete', $usuario) }}" method="POST">

                            @csrf
                            @method('DELETE')

                            <div class="mb-3">
                                <label for="">Nombres</label>
                                <input type="text" name="name" class="form-control" autocomplete="off" value="{{ $usuario->name }}" readonly>
                            </div>

                            <div class="mb-3">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" autocomplete="off" value="{{ $usuario->email }}" readonly>
                            </div>

                            <div class="mb-3">
                                <label for="">Rol</label>
                                <select name="rol" class="form-select" required>
                                    <option value="{{ $usuario->rol }}">{{ $usuario->rol }}</option>
                                    <option value="">--- Seleccionar ---</option>
                                    <option value="Estudiante">Estudiante</option>
                                    <option value="Aadministrador">Administrador</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                        <input type="submit" class="btn btn-danger text-white" value="Eliminar">
                                    
                                        <a href="{{ route('usuarios.index') }}" class="btn btn-secondary">Volver</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection