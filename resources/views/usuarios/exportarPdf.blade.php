<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF</title>

    
    
</head>
<body>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                Usuarios
            </div>
        </div>
    </div>
    <br>
    <table class="table" border="1">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre de usuario</th>
                <th>Correo electrónico</th>
                
                <th>Rol</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $key => $usuario)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $usuario->name }}</td>
                    <td>{{ $usuario->email }}</td>
                    
                    <td>{{ $usuario->rol }}</td>
                </tr>
            @endforeach                             
        </tbody>
    </table>
</body>
</html>